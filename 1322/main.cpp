#include <cstdio>
#include <iostream>
#include <string>

struct symbol {
    char c;
    int pos;
};

int compare(const void * a, const void * b){
    symbol s1 = *(symbol*) a;
    symbol s2 = *(symbol*) b;
    return (s1.c == s2.c) ? s1.pos - s2.pos : s1.c - s2.c;
}

int main(){
    int k;
    std::string s;
    
    scanf("%d", &k);
    std::cin >> s;

    k--;
    const int n = s.length();

    symbol a[n]; 

    for(int i = 0; i < n; i++){
        a[i].c = s[i];
        a[i].pos = i;
    }

    qsort(a, n, sizeof(symbol), compare);

    int cur = k;
    for(int i = 0; i < n; i++){
        printf("%c", a[cur].c);
        cur = a[cur].pos;
    }

    return 0;
}
