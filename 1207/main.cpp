#include <cstdio>
#include <iostream>
#include <algorithm>
#include <math.h>

#define PI_2 1.57079632679489661923
#define ll long long

struct Point
{
    int id;
    ll x;
    ll y;
    double angle;
};

int compare(const void * a, const void * b){
    Point p1 = *(Point*) a;
    Point p2 = *(Point*) b;
    return p1.angle > p2.angle ? 1 : -1;
}

bool compare_2(Point &a, Point &b){
    return a.angle < b.angle; 
}

int main(){
    int n;
    scanf("%d", &n);

    Point a[n];

    int leftIndex = 0;
    ll minX = 1e10l;

    for(int i = 0; i < n; i++){
        ll x, y;
        std::cin >> x >> y;

        if(minX > x){
            minX = x;
            leftIndex = i+1;
        }

        a[i].id = i+1;
        a[i].x  = x;
        a[i].y  = y;
    }
    const Point &left = a[leftIndex-1];
    for(int i = 0; i < n; i++){
        Point &p = a[i];
        if(p.id == leftIndex){
            p.angle = -__FLT_MAX__;
        }else if(p.x == left.x){
            p.angle = (p.y > left.y) ? PI_2 : -PI_2;
        }else{
            p.angle = atan((double)(p.y - left.y)/(p.x - left.x));
        }
    }

    int k = sizeof(a) / sizeof(a[0]); 
    std::sort(a, a+k, compare_2);
    // qsort(a, n, sizeof(Point), compare);

    printf("%d %d", leftIndex, a[n >> 1].id);
    return 0;
}