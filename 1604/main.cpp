#include <cstdio>
#include <cmath>

struct Sign{
    int id;
    int count;
};

int compare(const void * a, const void * b){
    Sign s1 = *(Sign*) a;
    Sign s2 = *(Sign*) b;
    return s2.count - s1.count;
}

int main(){
    int k;
    scanf("%d\n", &k);

    Sign s[k];
    int sum = 0;

    for(int i = 0; i < k; i++){
        int c;
        scanf("%d", &c);
        s[i].id = i+1;
        s[i].count = c;
        sum += c;
    }

    qsort(s, k, sizeof(Sign), compare);

    int ans[sum];

    bool h = s[0].count > ((sum+1) >> 1);
    bool fin = false;
    int j = 0;
    int i = 0;

    if(h){
        j = 1;
        i = k - 1;
    }

    while(j < sum){
        ans[j] = s[i].id;
        s[i].count--;

        if(s[i].count == 0){
            if(h) i--;
            else  i++;
        }

        j += 2;

        if(j > sum-1 && !fin){
            j = h ? 0 : 1; 
            fin = true;
        }
    }

    for(int p = 0; p < sum; p++){
        printf("%d ", ans[p]);
    }

    return 0;
}
