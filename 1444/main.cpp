#include <cstdio>
#include <cmath>

#define PI 3.14159265358979323846
#define PI_2 1.57079632679489661923
#define EPS 1e-10

struct Point
{
    int id;
    int x;
    int y;
    double angle;
    double len;
};

int compare(const void * a, const void * b){
    Point p1 = *(Point*) a;
    Point p2 = *(Point*) b;
    if(fabs(p1.angle - p2.angle) > EPS){
        return p1.angle > p2.angle ? 1 : -1;
    }
    return p1.len > p2.len ? 1 : -1;
}

double getLen(const Point &a1, const Point &a2){
    return fabs(sqrt((double) (pow((a1.x - a2.x), 2) + pow((a1.y - a2.y), 2))));
}

int main(){
    int N;
    scanf("%d", &N);

    Point a[N];

    for(int i = 0; i < N; i++){
        Point &cur = a[i];
        int x,y;
        scanf("%d %d", &x, &y);
        cur.id = i+1;
        cur.x = x;
        cur.y = y;

        if(i == 0){
            cur.angle = -__INT_MAX__;
            cur.len = 0;
            continue;
        }

        if(cur.x == a[0].x){
            cur.angle = (cur.y > a[0].y) ? PI_2 : -PI_2;
            cur.len = abs(cur.y - a[0].y);
            continue;
        }

        cur.angle = atan((double)(cur.y - a[0].y)/(cur.x - a[0].x));

        if(cur.x <= a[0].x) {
            cur.angle -= PI;
        }

        cur.len = getLen(cur, a[0]);
    }

    qsort(a, N, sizeof(Point), compare);

    double maxA = PI * 2 + a[1].angle - a[N-1].angle;
    int k = 1;

    for(int i = 1; i < N-1; i++){
        if(a[i+1].angle - a[i].angle > maxA){
            maxA = a[i+1].angle - a[i].angle;
            k = i+1;
        }
    }

    printf("%d\n%d\n", N, 1);

    for(int i = k; i < N; i++){
        printf("%d\n", a[i].id);
    }

    for(int i = 1; i < k; i++){
        printf("%d\n", a[i].id);
    }

    return 0;
}
